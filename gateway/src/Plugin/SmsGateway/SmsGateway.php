<?php
/*
* Contains Drupal\gateway\Plugin\SmsGateway
*/
namespace Drupal\gateway\Plugin\SmsGateway;

use \Drupal\sms\Plugin\SmsGatewayPluginInterface;
use \Drupal\sms\Plugin\SmsGatewayPluginBase;
use Drupal\sms\Message\SmsMessageInterface;


/**
 * @SmsGateway(
 *   id = "my_gateway",
 *   label = @Translation("My custom Gateway"),
 *   outgoing_message_max_recipients = -1,
 *   incoming = TRUE,
 *   incoming_route = TRUE,
 * )
 */

 class SmsGateWay extends SmsGatewayPluginBase implements SmsGatewayPluginInterface{
      /**
   * Callback for processing incoming messages.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request The active request.
   * @param \Drupal\sms\Entity\SmsGatewayInterface $sms_gateway The SMS gateway.
   *
   * @return \Drupal\sms\SmsProcessingResponse The processing response.
   */

    public function processIncoming(\Symfony\Component\HttpFoundation\Request $request, \Drupal\sms\Entity\SmsGatewayInterface $sms_gateway) {

        //constructing your own SmsMessage objects.
        $message = \Drupal\sms\Entity\SmsMessage::create();
        //properties must be set on a SmsMessage in order to be considered valid by the queuing system
        $message
        ->setDirection(\Drupal\sms\Direction::INCOMING)
        ->setMessage('The incoming message')//$request->body
        ->addRecipients(['+123456789']) // $request->phone A phone number, usually one representing the site.
        ->setGateway($sms_gateway);//$sms_gateway

        //result must be attached to each message
        $result = new \Drupal\sms\Message\SmsMessageResult();
        //report for each recipient must be created for each recipient of the message
        $report = (new \Drupal\sms\Message\SmsDeliveryReport())
        ->setRecipient('+123456789')// $request->phone
        ->setStatus(\Drupal\sms\Message\SmsMessageReportStatus::CODE)
        ->setMessageId('abc-def-ghi-jkl'); // $message->id An optional unique message ID.

        //this step from twilio module
        $result->addReport($report);
        $message->setResult($result);

        //This object contains the incoming messages that were found in the HTTP request, and the response
        $response = new \Symfony\Component\HttpFoundation\Response('', 204);//suppose to add$messageinstead of ''
        $task = (new \Drupal\sms\SmsProcessingResponse())
          ->setResponse($response)
          ->setMessages($message);
        return $task;

    }

    /**
   * {@inheritdoc}
   */
    public function send(SmsMessageInterface $sms_message) {

        //will need to load in the entity you wish to send to
        $entity = \Drupal\user\Entity\User::load(123);//$sms_message->getRecipients()

        /** @var \Drupal\sms\Provider\SmsProviderInterface $sms_service */
        $sms_service = \Drupal::service('sms.provider');

        //CONSTRUCT your SMS message
        $sms = (new \Drupal\sms\Message\SmsMessage())
        ->setMessage('Foobar') // Set the message.
        ->addRecipient('123456789') // Set recipient phone number
        ->setDirection(\Drupal\sms\Direction::OUTGOING);

        try {
            $sms_service->queue($sms);
          }
          catch (\Drupal\sms\Exception\RecipientRouteException $e) {
            // Thrown if no gateway could be determined for the message.
          }
          catch (\Exception $e) {
            // Other exceptions can be thrown.
          }

        $result = new SmsMessageResult();
        $reports = [];

        foreach ($sms_message->getRecipients() as $recipient) {
            // ...Send message with API
            $reports[] = (new SmsDeliveryReport())
            ->setRecipient($recipient)
            ->setStatus(SmsMessageReportStatus::SOMESTATUS);
        }

        $result->setReports($reports);
        return $result;

    }
 }

?>